package com.ruoyi.common.core.domain.model;

import cn.hutool.crypto.asymmetric.KeyType;
import cn.hutool.crypto.asymmetric.RSA;
import com.ruoyi.common.constant.Constants;

/**
 * 用户登录对象
 * 
 * @author ruoyi
 */
public class LoginBody
{
    /**
     * 用户名
     */
    private String username;

    /**
     * 用户密码
     */
    private String password;

    /**
     * 验证码
     */
    private String code;

    /**
     * 唯一标识
     */
    private String uuid;

    public String getUsername()
    {
        RSA rsa = new RSA(Constants.PRIVATE_KEY,null);
        return rsa.decryptStr(username, KeyType.PrivateKey);
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        RSA rsa = new RSA(Constants.PRIVATE_KEY,null);
        return rsa.decryptStr(password, KeyType.PrivateKey);
    }

    public void setPassword(String password)
    {
        this.password = password;
    }
    //不加密
    public String getUsername2(){
        return username;
    }
    //不加密
    public String getPassword2(){
        return password;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getUuid()
    {
        return uuid;
    }

    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }
}
