package com.ruoyi.common.core.domain;

import com.ruoyi.common.constant.HttpStatus;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value = "返回数据子类对象")
@Data
public class SuccessResponseData<T> extends ResponseData {
    @ApiModelProperty(value = "角色主键")
    private String roleIds;
    @ApiModelProperty(value = "部门主键")
    private String deptIds;

    public static <T> SuccessResponseData success1(T data) {
        SuccessResponseData<T> success = success1();
        success.setData(data);
        return success;
    }

    public static <T> SuccessResponseData success2(T data, String roleIds, String deptIds) {
        SuccessResponseData<T> success = success1();
        success.setData(data);
        success.setRoleIds(roleIds);
        success.setDeptIds(deptIds);
        return success;
    }


    public static <T> SuccessResponseData success1() {
        SuccessResponseData<T> success = new SuccessResponseData<T>();
        success.setCode(HttpStatus.SUCCESS);
        success.setMsg("请求成功");
        success.setSuccess(true);
        success.setData(null);
        return success;
    }
}
