package com.ruoyi.common.core.domain;

import com.ruoyi.common.constant.HttpStatus;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 转换为能够被swagger2
 * @param <T>
 */
@ApiModel(value = "返回对象")
@Data
public class ResponseDataCommon<T> extends ResponseData {
    @ApiModelProperty(value = "文件名")
    private String fileName;
    @ApiModelProperty(value = "上传记录主键")
    private Long uploadRecordId;
    @ApiModelProperty(value = "文件链接")
    private String url;

    public static <T> ResponseDataCommon successCommon(T data) {
        ResponseDataCommon<T> success = successCommon();
        success.setData(data);
        return success;
    }

    public static <T> ResponseDataCommon successCommon(Long uploadRecordId,String fileName, String url) {
        ResponseDataCommon<T> success = successCommon();
        success.setUploadRecordId(uploadRecordId);
        success.setFileName(fileName);
        success.setUrl(url);
        return success;
    }

    public static <T> ResponseDataCommon errorCommon(String msg) {
        ResponseDataCommon<T> error = errorCommon();
        error.setMsg(msg);
        return error;
    }

    public static <T> ResponseDataCommon successCommon() {
        ResponseDataCommon<T> success = new ResponseDataCommon<T>();
        success.setCode(HttpStatus.SUCCESS);
        success.setMsg("请求成功");
        success.setSuccess(true);
        success.setData(null);
        return success;
    }

    public static <T> ResponseDataCommon errorCommon() {
        ResponseDataCommon<T> success = new ResponseDataCommon<T>();
        success.setCode(HttpStatus.ERROR);
        success.setMsg("操作失败");
        success.setSuccess(false);
        success.setData(null);
        return success;
    }
}
