package com.ruoyi.common.core.domain;

import com.ruoyi.common.constant.HttpStatus;
import com.ruoyi.common.core.domain.entity.SysPost;
import com.ruoyi.common.core.domain.entity.SysRole;
import com.ruoyi.common.core.domain.entity.SysUser;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 转换为能够被swagger2
 * @param <T>
 */
@ApiModel(value = "用户授权角色")
@Data
public class ResponseDataUserAuthRoles<T> extends ResponseData {
    @ApiModelProperty(value = "当前用户授权角色")
    private List<SysRole> roles;
    @ApiModelProperty(value = "当前用户")
    private SysUser user;

    public static <T> ResponseDataUserAuthRoles successUser(T data) {
        ResponseDataUserAuthRoles<T> success = successUserAuthRoles();
        success.setData(data);
        return success;
    }

    public static <T> ResponseDataUserAuthRoles successUser(T data, List<SysRole> roles, SysUser user) {
        ResponseDataUserAuthRoles<T> success = successUserAuthRoles();
        success.setData(data);
        success.setRoles(roles);
        success.setUser(user);
        return success;
    }

    public static <T> ResponseDataUserAuthRoles successUserAuthRoles() {
        ResponseDataUserAuthRoles<T> success = new ResponseDataUserAuthRoles<T>();
        success.setCode(HttpStatus.SUCCESS);
        success.setMsg("请求成功");
        success.setSuccess(true);
        success.setData(null);
        return success;
    }
}
