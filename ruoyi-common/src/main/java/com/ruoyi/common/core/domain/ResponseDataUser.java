package com.ruoyi.common.core.domain;

import com.ruoyi.common.constant.HttpStatus;
import com.ruoyi.common.core.domain.entity.SysPost;
import com.ruoyi.common.core.domain.entity.SysRole;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 转换为能够被swagger2
 * @param <T>
 */
@ApiModel(value = "返回菜单数据子类对象")
@Data
public class ResponseDataUser<T> extends ResponseData {
    @ApiModelProperty(value = "当前用户可选角色信息")
    private List<SysRole> roles;
    @ApiModelProperty(value = "当前用户可选岗位信息")
    private List<SysPost> posts;
    @ApiModelProperty(value = "当前用户角色主键")
    private List<Long> roleIds;
    @ApiModelProperty(value = "当前用户岗位主键")
    private List<Long> postIds;

    public static <T> ResponseDataUser successUser(T data) {
        ResponseDataUser<T> success = successUser();
        success.setData(data);
        return success;
    }

    public static <T> ResponseDataUser successUser(T data, List<SysRole> roles, List<SysPost> posts) {
        ResponseDataUser<T> success = successUser();
        success.setData(data);
        success.setRoles(roles);
        success.setPosts(posts);
        return success;
    }

    public static <T> ResponseDataUser successUser(List<SysRole> roles, List<SysPost> posts) {
        ResponseDataUser<T> success = successUser();
        success.setRoles(roles);
        success.setPosts(posts);
        return success;
    }

    public static <T> ResponseDataUser successUser() {
        ResponseDataUser<T> success = new ResponseDataUser<T>();
        success.setCode(HttpStatus.SUCCESS);
        success.setMsg("请求成功");
        success.setSuccess(true);
        success.setData(null);
        return success;
    }
}
