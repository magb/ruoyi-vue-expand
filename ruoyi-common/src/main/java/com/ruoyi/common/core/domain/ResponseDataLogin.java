package com.ruoyi.common.core.domain;

import com.ruoyi.common.constant.HttpStatus;
import com.ruoyi.common.core.domain.entity.SysUser;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Set;

/**
 * 转换为能够被swagger2
 * @param <T>
 */
@ApiModel(value = "返回菜单数据子类对象")
@Data
public class ResponseDataLogin<T> extends ResponseData {
    @ApiModelProperty(value = "令牌")
    private String token;
    @ApiModelProperty(value = "用户信息")
    private SysUser user;
    @ApiModelProperty(value = "角色信息")
    private Set<String> roles;
    @ApiModelProperty(value = "权限信息")
    private Set<String> permissions;

    public static <T> ResponseDataLogin successLogin(T data) {
        ResponseDataLogin<T> success = successLogin();
        success.setData(data);
        return success;
    }

    public static <T> ResponseDataLogin successLogin(String token) {
        ResponseDataLogin<T> success = successLogin();
        success.setToken(token);
        return success;
    }

    public static <T> ResponseDataLogin successLogin(SysUser user, Set<String> roles, Set<String> permissions) {
        ResponseDataLogin<T> success = successLogin();
        success.setUser(user);
        success.setRoles(roles);
        success.setPermissions(permissions);
        return success;
    }


    public static <T> ResponseDataLogin successLogin() {
        ResponseDataLogin<T> success = new ResponseDataLogin<T>();
        success.setCode(HttpStatus.SUCCESS);
        success.setMsg("请求成功");
        success.setSuccess(true);
        success.setData(null);
        return success;
    }
}
