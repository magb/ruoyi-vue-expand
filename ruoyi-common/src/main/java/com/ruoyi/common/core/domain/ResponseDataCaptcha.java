package com.ruoyi.common.core.domain;

import com.ruoyi.common.constant.HttpStatus;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 转换为能够被swagger2
 * @param <T>
 */
@ApiModel(value = "返回菜单数据子类对象")
@Data
public class ResponseDataCaptcha<T> extends ResponseData {
    @ApiModelProperty(value = "唯一标识")
    private String uuid;
    @ApiModelProperty(value = "Base64编码图片")
    private String img;


    public static <T> ResponseDataCaptcha successCaptcha(T data) {
        ResponseDataCaptcha<T> success = successCaptcha();
        success.setData(data);
        return success;
    }

    public static <T> ResponseDataCaptcha successCaptcha(String uuid, String img) {
        ResponseDataCaptcha<T> success = successCaptcha();
        success.setUuid(uuid);
        success.setImg(img);
        return success;
    }

    public static <T> ResponseDataCaptcha errorCaptcha(String msg) {
        ResponseDataCaptcha<T> error = errorCaptcha();
        error.setMsg(msg);
        return error;
    }

    public static <T> ResponseDataCaptcha successCaptcha() {
        ResponseDataCaptcha<T> success = new ResponseDataCaptcha<T>();
        success.setCode(HttpStatus.SUCCESS);
        success.setMsg("请求成功");
        success.setSuccess(true);
        success.setData(null);
        return success;
    }

    public static <T> ResponseDataCaptcha errorCaptcha() {
        ResponseDataCaptcha<T> success = new ResponseDataCaptcha<T>();
        success.setCode(HttpStatus.ERROR);
        success.setMsg("操作失败");
        success.setSuccess(false);
        success.setData(null);
        return success;
    }
}
