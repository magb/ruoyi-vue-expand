package com.ruoyi.jimureport;

import org.jeecg.modules.jmreport.api.JmReportTokenServiceI;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * 积木报表
 *
 * 该配置可删除,1.5.2版本不支持多租户,token校验不足以支撑多租户,需用另外的模式实现租户报表隔离(可参考系统内的ureport)
 */
@Component
public class JimuReportConfig implements JmReportTokenServiceI {
    @Override
    public String getUsername(String s) {
        return "admin";
    }

    @Override
    public Map<String, Object> getUserInfo(String token) {
        Map<String, Object> map = new HashMap<>();
        return map;
    }

    @Override
    public Boolean verifyToken(String s) {
        return true;
    }
}
