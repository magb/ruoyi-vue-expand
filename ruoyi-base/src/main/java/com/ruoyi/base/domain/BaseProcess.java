package com.ruoyi.base.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import lombok.Data;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * Snaker流程对象 base_process
 * 
 * @author ruoyi
 * @date 2022-10-17
 */
@Data
@ApiModel(value = "Snaker流程对象")
public class BaseProcess extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @ApiModelProperty(value = "主键")
    private Long id;

    /** 流程名称 */
    @Excel(name = "流程名称")
    @ApiModelProperty(value = "流程名称")
    private String name;

    /** 流程显示名称 */
    @Excel(name = "流程显示名称")
    @ApiModelProperty(value = "流程显示名称")
    private String displayName;

    /** 流程定义信息 */
    @Excel(name = "流程定义信息")
    @ApiModelProperty(value = "流程定义信息")
    private String wfProcessContent;

    /** 关联流程定义主键 */
    @Excel(name = "关联流程定义主键")
    @ApiModelProperty(value = "关联流程定义主键")
    private String wfProcessId;

    /** 当前流程状态 */
    @Excel(name = "当前流程状态")
    @ApiModelProperty(value = "当前流程状态")
    private String status;

    /** 创建人主键 */
    @Excel(name = "创建人主键")
    @ApiModelProperty(value = "创建人主键")
    private Long createUserId;

    /** 创建人姓名 */
    @Excel(name = "创建人姓名")
    @ApiModelProperty(value = "创建人姓名")
    private String createUserName;

    /** 更新人主键 */
    @Excel(name = "更新人主键")
    @ApiModelProperty(value = "更新人主键")
    private Long updateUserId;

    /** 更新人姓名 */
    @Excel(name = "更新人姓名")
    @ApiModelProperty(value = "更新人姓名")
    private String updateUserName;

    /** 启用禁用状态(字典通用是否) */
    @Excel(name = "启用禁用状态(字典通用是否)")
    @ApiModelProperty(value = "启用禁用状态(字典通用是否)")
    private String enableFlag;

    /** 删除状态(通用是否) */
    @Excel(name = "删除状态(通用是否)")
    @ApiModelProperty(value = "删除状态(通用是否)")
    private String deleteFlag;

    /** 备用字段1 */
    @Excel(name = "备用字段1")
    @ApiModelProperty(value = "备用字段1")
    private String spareParam1;

    /** 备用字段2 */
    @Excel(name = "备用字段2")
    @ApiModelProperty(value = "备用字段2")
    private String spareParam2;

    /** 备用字段3 */
    @Excel(name = "备用字段3")
    @ApiModelProperty(value = "备用字段3")
    private String spareParam3;

    /** 备用字段4 */
    @Excel(name = "备用字段4")
    @ApiModelProperty(value = "备用字段4")
    private String spareParam4;

    /** 备用字段5 */
    @Excel(name = "备用字段5")
    @ApiModelProperty(value = "备用字段5")
    private String spareParam5;

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("displayName", getDisplayName())
            .append("wfProcessContent", getWfProcessContent())
            .append("wfProcessId", getWfProcessId())
            .append("status", getStatus())
            .append("createUserId", getCreateUserId())
            .append("createUserName", getCreateUserName())
            .append("createTime", getCreateTime())
            .append("updateUserId", getUpdateUserId())
            .append("updateUserName", getUpdateUserName())
            .append("updateTime", getUpdateTime())
            .append("enableFlag", getEnableFlag())
            .append("deleteFlag", getDeleteFlag())
            .append("remark", getRemark())
            .append("spareParam1", getSpareParam1())
            .append("spareParam2", getSpareParam2())
            .append("spareParam3", getSpareParam3())
            .append("spareParam4", getSpareParam4())
            .append("spareParam5", getSpareParam5())
            .toString();
    }
}
