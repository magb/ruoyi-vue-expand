package com.ruoyi.base.service.impl;

import com.ruoyi.base.domain.BaseExportTemp;
import com.ruoyi.base.mapper.BaseExportTempMapper;
import com.ruoyi.base.service.IBaseExportTempService;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.SnowFlakeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * ureport2模板管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-01-21
 */
@Service
@Transactional
public class BaseExportTempServiceImpl implements IBaseExportTempService 
{
    @Autowired
    private BaseExportTempMapper baseExportTempMapper;

    /**
     * 查询ureport2模板管理
     * 
     * @param id ureport2模板管理主键
     * @return ureport2模板管理
     */
    @Override
    public BaseExportTemp selectBaseExportTempById(Long id)
    {
        return baseExportTempMapper.selectBaseExportTempById(id);
    }

    /**
     * 查询ureport2模板管理列表
     * 
     * @param baseExportTemp ureport2模板管理
     * @return ureport2模板管理
     */
    @Override
    public List<BaseExportTemp> selectBaseExportTempList(BaseExportTemp baseExportTemp)
    {
        return baseExportTempMapper.selectBaseExportTempList(baseExportTemp);
    }

    /**
     * 新增ureport2模板管理
     * 
     * @param baseExportTemp ureport2模板管理
     * @return 结果
     */
    @Override
    public int insertBaseExportTemp(BaseExportTemp baseExportTemp)
    {
        if(baseExportTemp.getId()==null){
        baseExportTemp.setId(SnowFlakeUtils.snowFlakeId());
    }
        baseExportTemp.setCreateUserId(SecurityUtils.getUserId());
        baseExportTemp.setCreateUserName(SecurityUtils.getUserNickName());
        baseExportTemp.setCreateTime(DateUtils.getNowDate());
        return baseExportTempMapper.insertBaseExportTemp(baseExportTemp);
    }

    /**
     * 修改ureport2模板管理
     * 
     * @param baseExportTemp ureport2模板管理
     * @return 结果
     */
    @Override
    public int updateBaseExportTemp(BaseExportTemp baseExportTemp)
    {
        baseExportTemp.setUpdateUserId(SecurityUtils.getUserId());
        baseExportTemp.setUpdateUserName(SecurityUtils.getUserNickName());
        baseExportTemp.setUpdateTime(DateUtils.getNowDate());
        return baseExportTempMapper.updateBaseExportTemp(baseExportTemp);
    }

    /**
     * 批量删除ureport2模板管理
     * 
     * @param ids 需要删除的ureport2模板管理主键
     * @return 结果
     */
    @Override
    public int deleteBaseExportTempByIds(Long[] ids)
    {
        return baseExportTempMapper.deleteBaseExportTempByIds(ids);
    }

    /**
     * 删除ureport2模板管理信息
     * 
     * @param id ureport2模板管理主键
     * @return 结果
     */
    @Override
    public int deleteBaseExportTempById(Long id)
    {
        return baseExportTempMapper.deleteBaseExportTempById(id);
    }

    /**
     * 批量---逻辑删除
     *
     * @param ids 需要删除的ureport2模板管理ID
     * @return 结果
     */
    @Override
    public int setDeleteByIds(Long[] ids)
    {
        Long userId = SecurityUtils.getUserId();
        String userNickName = SecurityUtils.getUserNickName();
        return baseExportTempMapper.setDeleteByIds(ids,userId,userNickName);
    }

    /**
     * 删除---逻辑删除
     *
     * @param id ureport2模板管理ID
     * @return 结果
     */
    @Override
    public int setDeleteById(Long id)
    {
        Long userId = SecurityUtils.getUserId();
        String userNickName = SecurityUtils.getUserNickName();
        return baseExportTempMapper.setDeleteById(id,userId,userNickName);
    }

    /**
     * 根据code查询参数
     * @param code
     * @return
     */
    public BaseExportTemp selectParamByCode(String code){
        return baseExportTempMapper.selectParamByCode(code);
    }

    /**
     * 根据模板的存放路径查询唯一的模板信息
     * @param fullPath
     * @return
     */
    public BaseExportTemp selectSignleByFullPath(String fullPath){
        return baseExportTempMapper.selectSignleByFullPath(fullPath);
    }
}
