package com.ruoyi.base.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.base.domain.BaseProcess;
import org.snaker.engine.access.Page;
import org.snaker.engine.entity.HistoryTask;
import org.snaker.engine.entity.Order;
import org.snaker.engine.entity.Process;
import org.snaker.engine.entity.Task;
import org.snaker.engine.entity.WorkItem;
import org.snaker.engine.model.TaskModel;

/**
 * Snaker流程Service接口
 * 
 * @author ruoyi
 * @date 2022-10-17
 */
public interface IBaseProcessService 
{
    /**
     * 查询Snaker流程
     * 
     * @param id Snaker流程主键
     * @return Snaker流程
     */
    public BaseProcess selectBaseProcessById(Long id);

    /**
     * 查询Snaker流程列表
     * 
     * @param baseProcess Snaker流程
     * @return Snaker流程集合
     */
    public List<BaseProcess> selectBaseProcessList(BaseProcess baseProcess);

    /**
     * 新增Snaker流程
     * 
     * @param baseProcess Snaker流程
     * @return 结果
     */
    public int insertBaseProcess(BaseProcess baseProcess);

    /**
     * 修改Snaker流程
     * 
     * @param baseProcess Snaker流程
     * @return 结果
     */
    public int updateBaseProcess(BaseProcess baseProcess);

    /**
     * 批量删除Snaker流程
     * 
     * @param ids 需要删除的Snaker流程主键集合
     * @return 结果
     */
    public int deleteBaseProcessByIds(Long[] ids);

    /**
     * 删除Snaker流程信息
     * 
     * @param id Snaker流程主键
     * @return 结果
     */
    public int deleteBaseProcessById(Long id);

    /**
     * 批量删除---逻辑删除
     *
     * @param ids 需要删除的Snaker流程ID
     * @return 结果
     */
    public int setDeleteByIds(Long[] ids);

    /**
     * 删除---逻辑删除
     *
     * @param id Snaker流程ID
     * @return 结果
     */
    public int setDeleteById(Long id);

    /**
     * 发布流程
     * @param id
     * @return
     */
    public int processPublish(Long id);

    /**
     * 发起流程
     * @param id
     * @return
     */
    public Order processStart(Long id, Map<String,Object> params);
    /**
     * 查询当前用户完成的任务
     * @return
     */
    public void getUserHistoryTasks(Page<HistoryTask> page);
    /**
     * 查询实例的已完成任务
     * @param orderId
     * @return
     */
    public List<HistoryTask> getHistoryTasks(String orderId);
    /**
     * 查询实例正在进行的任务
     * @param orderId
     * @return
     */
    public List<Task> getActiveTasks(String orderId);
    /**
     * 查询用户待办任务
     * @param page
     */
    public void getUserTask(Page<WorkItem> page);
    /**
     * 给实例的指定任务节点添加操作人
     * @param orderId
     * @param taskName
     * @param operator
     */
    public void addTaskActor(String orderId, String taskName, String operator);
    /**
     * 查询实例,指定任务的操作人信息
     * @param orderId
     * @param taskName
     * @return
     */
    public List<String> getTaskActor(String orderId, String taskName);
    /**
     * 查询流程定义信息
     * @param processId
     * @return
     */
    public Process getProcessById(String processId);
    /**
     * 查询流程的所有节点信息
     * @param processId
     * @return
     */
    public List<TaskModel> getProcessNodes(String processId);
}
