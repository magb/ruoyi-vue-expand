package com.ruoyi.base.service;

import java.util.List;
import com.ruoyi.base.domain.BaseExportTemp;

/**
 * ureport2模板管理Service接口
 * 
 * @author ruoyi
 * @date 2022-01-21
 */
public interface IBaseExportTempService 
{
    /**
     * 查询ureport2模板管理
     * 
     * @param id ureport2模板管理主键
     * @return ureport2模板管理
     */
    public BaseExportTemp selectBaseExportTempById(Long id);

    /**
     * 查询ureport2模板管理列表
     * 
     * @param baseExportTemp ureport2模板管理
     * @return ureport2模板管理集合
     */
    public List<BaseExportTemp> selectBaseExportTempList(BaseExportTemp baseExportTemp);

    /**
     * 新增ureport2模板管理
     * 
     * @param baseExportTemp ureport2模板管理
     * @return 结果
     */
    public int insertBaseExportTemp(BaseExportTemp baseExportTemp);

    /**
     * 修改ureport2模板管理
     * 
     * @param baseExportTemp ureport2模板管理
     * @return 结果
     */
    public int updateBaseExportTemp(BaseExportTemp baseExportTemp);

    /**
     * 批量删除ureport2模板管理
     * 
     * @param ids 需要删除的ureport2模板管理主键集合
     * @return 结果
     */
    public int deleteBaseExportTempByIds(Long[] ids);

    /**
     * 删除ureport2模板管理信息
     * 
     * @param id ureport2模板管理主键
     * @return 结果
     */
    public int deleteBaseExportTempById(Long id);

    /**
     * 批量删除---逻辑删除
     *
     * @param ids 需要删除的ureport2模板管理ID
     * @return 结果
     */
    public int setDeleteByIds(Long[] ids);

    /**
     * 删除---逻辑删除
     *
     * @param id ureport2模板管理ID
     * @return 结果
     */
    public int setDeleteById(Long id);

    /**
     * 根据code查询参数
     * @param code
     * @return
     */
    public BaseExportTemp selectParamByCode(String code);

    /**
     * 根据模板的存放路径查询唯一的模板信息
     * @param fullPath
     * @return
     */
    public BaseExportTemp selectSignleByFullPath(String fullPath);
}
