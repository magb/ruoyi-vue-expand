package com.ruoyi.base;

import org.snaker.engine.Assignment;
import org.snaker.engine.core.Execution;
import org.snaker.engine.model.TaskModel;
import org.springframework.stereotype.Component;

/**
 * 任务参与者处理
 */
@Component
public class SnakerAssignmentHander extends Assignment {
    @Override
    public Object assign(TaskModel taskModel, Execution execution) {
        return new String[]{"admin","test"};
    }
}
