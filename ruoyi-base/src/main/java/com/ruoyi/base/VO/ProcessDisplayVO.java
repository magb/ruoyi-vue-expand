package com.ruoyi.base.VO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.snaker.engine.entity.Process;
import org.snaker.engine.model.ProcessModel;

import java.util.List;

@Data
@ApiModel(value = "流程进度展示数据")
public class ProcessDisplayVO {
    @ApiModelProperty("流程定义")
    private String processInfo;
    @ApiModelProperty("已完成节点")
    private List<String> historyNodeNames;
    @ApiModelProperty("已完成边")
    private List<String> historyEdgeNames;
    @ApiModelProperty("正在进行中节点")
    private List<String> activeNodeNames;
}
