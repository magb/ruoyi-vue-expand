package com.ruoyi.base.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.ruoyi.base.domain.BaseProcess;

/**
 * Snaker流程Mapper接口
 * 
 * @author ruoyi
 * @date 2022-10-17
 */
public interface BaseProcessMapper 
{
    /**
     * 查询Snaker流程
     * 
     * @param id Snaker流程主键
     * @return Snaker流程
     */
    public BaseProcess selectBaseProcessById(Long id);

    /**
     * 查询Snaker流程列表
     * 
     * @param baseProcess Snaker流程
     * @return Snaker流程集合
     */
    public List<BaseProcess> selectBaseProcessList(BaseProcess baseProcess);

    /**
     * 新增Snaker流程
     * 
     * @param baseProcess Snaker流程
     * @return 结果
     */
    public int insertBaseProcess(BaseProcess baseProcess);

    /**
     * 修改Snaker流程
     * 
     * @param baseProcess Snaker流程
     * @return 结果
     */
    public int updateBaseProcess(BaseProcess baseProcess);

    /**
     * 删除Snaker流程
     * 
     * @param id Snaker流程主键
     * @return 结果
     */
    public int deleteBaseProcessById(Long id);

    /**
     * 批量删除Snaker流程
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBaseProcessByIds(Long[] ids);
    /**
     * 删除Snaker流程
     *
     * @param id Snaker流程ID
     * @return 结果
     */
    public int setDeleteById(@Param("id")Long id,@Param("operId")Long operId,@Param("operName")String operName);

    /**
     * 批量删除Snaker流程---逻辑删除
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int setDeleteByIds(@Param("ids")Long[] ids,@Param("operId")Long operId,@Param("operName")String operName);
}
