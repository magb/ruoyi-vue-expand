import {parseTime} from "@/utils/ruoyi"
const comment = {
  state: {
    pageFlag:"",//页面标识---根据该参数判断是否是当前页面的评论发生变化
    random:"",//随机数---该数值变动,说明有评论产生
    newComment:"",//新评论内容---本demo中静态数据使用,实际环境中不需要
  },

  mutations: {
    SET_PAGE_FLAG: (state, pageFlag) => {
      state.pageFlag = pageFlag
    },
    SET_RANDOM: (state, random) => {
      state.random = random
    },
    SET_NEW_COMMENT: (state, newComment) => {
      state.newComment = newComment
    }
  },

  actions: {
    // 添加评论---业务信息根据实际情况补充
    AddComment({ commit,rootGetters }, obj) {
      return new Promise((resolve, reject) => {
        let random = Number.parseInt(Math.random()*10000000000);
        let newComment = {
          id:random,
          parentId:obj.parentId,
          content:obj.content,
          createDate:parseTime(new Date()),
          userId:rootGetters.userId,
          nickName:rootGetters.name,
          avatar:rootGetters.avatar,
        }
        commit("SET_PAGE_FLAG",obj.pageFlag);
        commit("SET_RANDOM",random);
        commit("SET_NEW_COMMENT",newComment);
      })
    },
  }
}

export default comment
