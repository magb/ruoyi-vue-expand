import request from '@/utils/request'

// 查询Snaker流程列表
export function listBaseProcess(query) {
  return request({
    url: '/baseProcess/baseProcess/list',
    method: 'get',
    params: query
  })
}

// 查询Snaker流程详细
export function getBaseProcess(id) {
  return request({
    url: '/baseProcess/baseProcess/' + id,
    method: 'get'
  })
}

// 新增Snaker流程
export function addBaseProcess(data) {
  return request({
    url: '/baseProcess/baseProcess',
    method: 'post',
    data: data
  })
}

// 修改Snaker流程
export function updateBaseProcess(data) {
  return request({
    url: '/baseProcess/baseProcess',
    method: 'put',
    data: data
  })
}

// 删除Snaker流程
export function delBaseProcess(id) {
  return request({
    url: '/baseProcess/baseProcess/' + id,
    method: 'delete'
  })
}

// 发布流程
export function processPublish(id) {
  return request({
    url: '/baseProcess/baseProcess/processPublish/' + id,
    method: 'put'
  })
}
// 查询流程的所有节点
export function processNodes(processId) {
  return request({
    url: '/baseProcess/baseProcess/processNodes/' + processId,
    method: 'get'
  })
}
// 发起流程
export function processStart(id,data) {
  return request({
    url: '/baseProcess/baseProcess/processStart/' + id,
    method: 'put',
    data:data
  })
}
// 查询用户待办任务
export function getUserTasks(param) {
  return request({
    url: '/baseProcess/baseProcess/getUserTasks',
    params:param,
    method: 'get'
  })
}
// 查询流程进度展示数据
export function getProcessDsiplay(param) {
  return request({
    url: '/baseProcess/baseProcess/getProcessDsiplay',
    params:param,
    method: 'get'
  })
}
