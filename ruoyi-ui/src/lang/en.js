export default{
  common:{
    tips:{
      change:"Please Select",
      input:"Please Input"
    }
  },
  project:{
    title:"RuoYi Management System"
  },
  login:{
    userName:"Account",
    userNameRule:"Account cannot be empty",
    password:"Password",
    passwordRule:"Password cannot be empty",
    language:"Language",
    languageRule:"Please select Language",
    rememberMe:"RememberMe",
    login:"Login",
    logining:"Login···",
    loginSuccess:"Login Success"
  }
}