export default{
  common:{
    tips:{
      change:"请选择",
      input:"请输入"
    }
  },
  project:{
    title:"若依后台管理系统"
  },
  login:{
    userName:"用户名",
    userNameRule:"用户名不能为空",
    password:"密码",
    passwordRule:"密码不能为空",
    language:"语言",
    languageRule:"请选择语言",
    rememberMe:"记住我",
    login:"登 录",
    logining:"登 录 中···",
    loginSuccess:"登陆成功"
  }
}