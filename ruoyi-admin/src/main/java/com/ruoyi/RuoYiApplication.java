package com.ruoyi;

import cn.hutool.json.JSONUtil;
import com.ruoyi.common.utils.spring.SpringUtils;
import com.ruoyi.constant.RabbitConsts;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;

/**
 * 启动程序
 * 
 * @author ruoyi
 */
//@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class})
//积木报表
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class,MongoAutoConfiguration.class },scanBasePackages = {"org.jeecg.modules.jmreport","com.ruoyi"})
public class RuoYiApplication
{
    public static void main(String[] args)
    {
        // System.setProperty("spring.devtools.restart.enabled", "false");
        SpringApplication.run(RuoYiApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  若依启动成功   ლ(´ڡ`ლ)ﾞ  \n" +
                " .-------.       ____     __        \n" +
                " |  _ _   \\      \\   \\   /  /    \n" +
                " | ( ' )  |       \\  _. /  '       \n" +
                " |(_ o _) /        _( )_ .'         \n" +
                " | (_,_).' __  ___(_ o _)'          \n" +
                " |  |\\ \\  |  ||   |(_,_)'         \n" +
                " |  | \\ `'   /|   `-'  /           \n" +
                " |  |  \\    /  \\      /           \n" +
                " ''-'   `'-'    `-..-'              ");
        RabbitTemplate rabbitTemplate = SpringUtils.getBean(RabbitTemplate.class);
        for (int i=0;i<10;i++){
            String json = "{'id':'你好"+i+"'}";
            rabbitTemplate.convertAndSend(RabbitConsts.TOPIC_MODE_QUEUE,RabbitConsts.DIRECT_MODE_QUEUE_ONE, JSONUtil.parseObj(json));
        }
    }
}
