package com.ruoyi.web.controller.base;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import com.ruoyi.base.DTO.BaseTempExportDTO;
import com.ruoyi.common.utils.DateUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.ruoyi.common.annotation.RepeatSubmit;
import com.ruoyi.common.core.domain.ResponseData;
import java.util.List;
import java.util.stream.Collectors;
import javax.validation.Valid;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.core.env.Environment;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.base.domain.BaseExportTemp;
import com.ruoyi.base.service.IBaseExportTempService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * ureport2模板管理Controller
 * 
 * @author ruoyi
 * @date 2022-01-21
 */
@Api(value = "ureport2模板管理",tags = "ureport2模板管理")
@ApiSupport(author = "ruoyi",order = 10)
@RestController
@RequestMapping("/base/baseExportTemp")
public class BaseExportTempController extends BaseController
{
    @Autowired
    private IBaseExportTempService baseExportTempService;

    @Autowired
    Environment environment;
    /**
     * 查询ureport2模板管理列表
     */
    @ApiOperation(value = "查询ureport2模板管理列表",notes = "查询ureport2模板管理列表")
    @ApiOperationSupport(order = 10)
    @PreAuthorize("@ss.hasPermi('base:baseExportTemp:list')")
    @GetMapping("/list")
    public TableDataInfo<BaseExportTemp> list(BaseExportTemp baseExportTemp)
    {
        startPage();
        List<BaseExportTemp> list = baseExportTempService.selectBaseExportTempList(baseExportTemp);
        return getDataTable(list);
    }

    /**
     * 导出ureport2模板管理列表
     */
    @ApiOperation(value = "导出ureport2模板管理列表",notes = "导出ureport2模板管理列表")
    @ApiOperationSupport(order = 20)
    @PreAuthorize("@ss.hasPermi('base:baseExportTemp:export')")
    @Log(title = "ureport2模板管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseExportTemp baseExportTemp)
    {
        List<BaseExportTemp> list = baseExportTempService.selectBaseExportTempList(baseExportTemp);
        ExcelUtil<BaseExportTemp> util = new ExcelUtil<BaseExportTemp>(BaseExportTemp.class);
        util.exportExcel(response, list, "ureport2模板管理数据");
    }

    /**
     * 获取ureport2模板管理详细信息
     */
    @ApiOperation(value = "获取ureport2模板管理详细信息",notes = "获取ureport2模板管理详细信息")
    @ApiOperationSupport(order = 30)
    @PreAuthorize("@ss.hasPermi('base:baseExportTemp:query')")
    @GetMapping(value = "/{id}")
    public ResponseData<BaseExportTemp> getInfo(@PathVariable("id") Long id)
    {
        return ResponseData.success(baseExportTempService.selectBaseExportTempById(id));
    }

    /**
     * 新增ureport2模板管理
     */
    @ApiOperation(value = "新增ureport2模板管理",notes = "新增ureport2模板管理")
    @ApiOperationSupport(order = 40,ignoreParameters = {"baseExportTemp.id"})
    @PreAuthorize("@ss.hasPermi('base:baseExportTemp:add')")
    @Log(title = "ureport2模板管理", businessType = BusinessType.INSERT)
    @PostMapping
    @RepeatSubmit
    public ResponseData add(@RequestBody @Valid @ApiParam(required=true) BaseExportTemp baseExportTemp)
    {
        int i = baseExportTempService.insertBaseExportTemp(baseExportTemp);
        if(i>0){
            //返回主键
            return ResponseData.success(baseExportTemp.getId());
        }else{
            return ResponseData.error();
        }
    }

    /**
     * 修改ureport2模板管理
     */
    @ApiOperation(value = "修改ureport2模板管理",notes = "修改ureport2模板管理")
    @ApiOperationSupport(order = 50)
    @PreAuthorize("@ss.hasPermi('base:baseExportTemp:edit')")
    @Log(title = "ureport2模板管理", businessType = BusinessType.UPDATE)
    @PutMapping
    @RepeatSubmit
    public ResponseData edit(@RequestBody @Valid @ApiParam(required=true) BaseExportTemp baseExportTemp)
    {
        int i = baseExportTempService.updateBaseExportTemp(baseExportTemp);
        if(i>0){
            //返回主键
            return ResponseData.success(baseExportTemp.getId());
        }else{
            return ResponseData.error();
        }
    }

    /**
     * 删除ureport2模板管理
     */
    @ApiOperation(value = "删除ureport2模板管理",notes = "删除ureport2模板管理")
    @ApiOperationSupport(order = 60)
    @PreAuthorize("@ss.hasPermi('base:baseExportTemp:remove')")
    @Log(title = "ureport2模板管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public ResponseData remove(@PathVariable Long[] ids)
    {
        //return toResult(baseExportTempService.deleteBaseExportTempByIds(ids));
        //逻辑删除
        return toResult(baseExportTempService.setDeleteByIds(ids));
    }

    /**
     * 查询ureport2模板管理列表---不进行分页
     */
    @ApiOperation(value = "查询ureport2模板管理列表---不进行分页",notes = "查询ureport2模板管理列表---不进行分页")
    @ApiOperationSupport(order = 70)
    @PreAuthorize("@ss.hasPermi('base:baseExportTemp:list')")
    @PostMapping("/getDataList")
    public ResponseData<List<BaseExportTemp>> getDataList(@RequestBody BaseExportTemp baseExportTemp)
    {
        List<BaseExportTemp> list = baseExportTempService.selectBaseExportTempList(baseExportTemp);
        return ResponseData.success(list);
    }

    /**
     * 跳转到添加模板页面
     * @return
     */
    @ApiOperation(value = "返回添加模板页面路径",notes = "返回添加模板页面路径")
    @ApiOperationSupport(order = 80)
    @GetMapping("/addTemp")
    @PreAuthorize("@ss.hasPermi('baseExportTemp:baseExportTemp:add')")
    public String addTemp(){
//        return "http://"+ IpUtils.getHostIp() +":"+environment.getProperty("server.port")+"/ureport/designer";
        return environment.getProperty("ruoyi.ureporturl")+"/ureport/designer";
    }

    /**
     * 跳转到修改模板页面
     * @return
     */
    @ApiOperation(value = "返回修改模板页面路径",notes = "返回修改模板页面路径")
    @ApiOperationSupport(order = 90)
    @GetMapping("/editTemp/{id}")
    @PreAuthorize("@ss.hasPermi('baseExportTemp:baseExportTemp:edit')")
    public String editTemp(@PathVariable Long id){
        BaseExportTemp baseExportTemp1 = baseExportTempService.selectBaseExportTempById(id);
        if(baseExportTemp1==null){
            throw new SecurityException("当前模板不存在");
        }
//        return "http://"+ IpUtils.getHostIp() +":"+environment.getProperty("server.port")+"/ureport/designer"+"?_u="+baseExportTemp1.getPrefix()+baseExportTemp1.getTempFileName();
        return environment.getProperty("ruoyi.ureporturl")+"/ureport/designer"+"?_u="+baseExportTemp1.getPrefix()+baseExportTemp1.getTempFileName();
    }

    /**
     * 跳转到预览模板页面
     * @return
     */
    @ApiOperation(value = "返回预览模板页面路径",notes = "返回预览模板页面路径")
    @ApiOperationSupport(order = 90)
    @GetMapping("/previwTemp/{id}")
    @PreAuthorize("@ss.hasPermi('baseExportTemp:baseExportTemp:list')")
    public String previwTemp(@PathVariable Long id){
        BaseExportTemp baseExportTemp1 = baseExportTempService.selectBaseExportTempById(id);
        if(baseExportTemp1==null){
            throw new SecurityException("当前模板不存在");
        }
//        return "http://"+ IpUtils.getHostIp() +":"+environment.getProperty("server.port")+"/ureport/preview?_u="+baseExportTemp1.getPrefix()+baseExportTemp1.getTempFileName();
        return environment.getProperty("ruoyi.ureporturl")+"/ureport/preview?_u="+baseExportTemp1.getPrefix()+baseExportTemp1.getTempFileName();
    }
    /**
     * 实际业务--跳转到预览模板页面
     * @return
     */
    @ApiOperation(value = "实际业务--跳转到预览模板页面",notes = "实际业务--跳转到预览模板页面")
    @ApiOperationSupport(order = 90)
    @GetMapping("/businessPreviwTemp")
    public String businessPreviwTemp(@Valid BaseTempExportDTO baseTempExportDTO){
        BaseExportTemp baseExportTemp1 = baseExportTempService.selectParamByCode(baseTempExportDTO.getCode());
        if(baseExportTemp1==null){
            throw new SecurityException("当前模板不存在");
        }
        String extraStr = "";
        if(StringUtils.isNotBlank(baseTempExportDTO.getOrderNo())){
            extraStr = extraStr+"&orderNo="+baseTempExportDTO.getOrderNo();
        }
        if(baseTempExportDTO.getExtraParams()!=null){
            String extraParams = baseTempExportDTO.getExtraParams().stream().collect(Collectors.joining(","));
            extraStr = extraStr+"&extraParams="+extraParams;
        }
//        return "http://"+ IpUtils.getHostIp() +":"
//                +environment.getProperty("server.port")+"/ureport/preview?_u="
//                +baseExportTemp1.getPrefix()+baseExportTemp1.getTempFileName()
//                +"&orderNo="+orderNo;
        //直接跳转到PDF预览页面
//        return "http://"+ IpUtils.getHostIp() +":"
//                +environment.getProperty("server.port")+"/ureport/pdf/show?_u="
//                +baseExportTemp1.getPrefix()+baseExportTemp1.getTempFileName()
//                +"&orderNo="+orderNo;
        return environment.getProperty("ruoyi.ureporturl")+"/ureport/pdf/show?_u="
                +baseExportTemp1.getPrefix()+baseExportTemp1.getTempFileName()
                +extraStr;
    }

    /**
     * 实际业务--导出excel
     * @return
     */
    @ApiOperation(value = "实际业务--导出excel",notes = "实际业务--导出excel")
    @ApiOperationSupport(order = 100)
    @GetMapping("/businessExportExcel")
    public String businessExportExcel(@Valid BaseTempExportDTO baseTempExportDTO){
        BaseExportTemp baseExportTemp1 = baseExportTempService.selectParamByCode(baseTempExportDTO.getCode());
        if(baseExportTemp1==null){
            throw new SecurityException("当前模板不存在");
        }
//        return "http://"+ IpUtils.getHostIp() +":"
//                +environment.getProperty("server.port")+"/ureport/preview?_u="
//                +baseExportTemp1.getPrefix()+baseExportTemp1.getTempFileName()
//                +"&orderNo="+orderNo;
        //直接跳转到PDF预览页面
//        return "http://"+ IpUtils.getHostIp() +":"
//                +environment.getProperty("server.port")+"/ureport/excel?_u="
//                +baseExportTemp1.getPrefix()+baseExportTemp1.getTempFileName()
//                +"&orderNo="+orderNo;
        String extraStr = "";
        if(StringUtils.isNotBlank(baseTempExportDTO.getOrderNo())){
            extraStr = extraStr+"&orderNo="+baseTempExportDTO.getOrderNo();
        }
        if(baseTempExportDTO.getBeginTime()!=null){
            extraStr = extraStr+"&beginTime="+ DateUtils.parseDateToStr("yyyy-MM-dd HH:mm:ss",baseTempExportDTO.getBeginTime());
        }
        if(baseTempExportDTO.getEndTime()!=null){
            extraStr = extraStr+"&endTime="+ DateUtils.parseDateToStr("yyyy-MM-dd HH:mm:ss",baseTempExportDTO.getEndTime());
        }
        if(baseTempExportDTO.getExtraParams()!=null){
            String extraParams = baseTempExportDTO.getExtraParams().stream().collect(Collectors.joining(","));
            extraStr = extraStr+"&extraParams="+extraParams;
        }
        return environment.getProperty("ruoyi.ureporturl")+"/ureport/excel?_u="
                +baseExportTemp1.getPrefix()+baseExportTemp1.getTempFileName()
                +extraStr;
    }
}
